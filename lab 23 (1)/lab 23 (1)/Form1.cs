﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_23__1_
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
		{// Введення шляху до файлу
			Console.WriteLine("Введіть шлях до файлу:");
			string filePath = Console.ReadLine();

			// Перевірка наявності файлу
			if (!File.Exists(filePath))
			{
				Console.WriteLine("Файл не знайдено.");
				Console.ReadLine();
				return;
			}

			// Зчитування чисел з файлу та пошук найбільшого елемента
			double maxNumber = double.MinValue;
			using (StreamReader reader = new StreamReader(filePath))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
				{
					double number;
					if (double.TryParse(line, out number))
					{
						if (number > maxNumber)
						{
							maxNumber = number;
						}
					}
				}
			}

			// Виведення результату
			Console.WriteLine("Найбільший елемент: " + maxNumber);



		}
	}
}
